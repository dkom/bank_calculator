<?php

namespace Drupal\deko\Service;

use Drupal\Core\Datetime\Element\Datetime;

/**
 * Class DeKoFinance.
 */
class DeKoFinance {

  public const PRESICION = 2;

  /**
   * Constructs a new DeKoFinance object.
   */
  public function __construct() {

  }

  /**
   * Calculate payment.
   */
  protected function payment(float $rate, int $periods, float $loanAmount): float {
    if ($rate) {
      $result = ($loanAmount * pow(1 + $rate, $periods)) / ((pow(1 + $rate, $periods) - 1) / $rate);
    } else {
      $result = ($loanAmount) / $periods;
    }

    return $result;
  }

  /**
   * Calculate list with data for payments.
   */
  public function prepareDataList(float $loanAmount, float $rate, int $years, int $paymentsPerYear, \DateTime $startDate, float $extra): array {
    $result = [];
    $periods = $years * $paymentsPerYear;
    $rate = $rate / 100;
    $originalPayment = $this->payment($rate/$paymentsPerYear, $periods, $loanAmount);
    $payment = round($originalPayment, self::PRESICION);
    $lostPayment = $originalPayment - $payment;

    if (12 % $paymentsPerYear == 0) {
      $plusInterval = new \DateInterval('P' . (12 / $paymentsPerYear) . 'M');
    }
    else {
      $plusInterval = new \DateInterval('P' . round(365 / $paymentsPerYear) . 'D');
    }

    $delta = 1;
    $endBalance = $loanAmount;
    $summaryInterest = 0;
    $totalPayment = $payment + $extra;
    $totalExtra = 0;
    $result['#rows'] = [];

    $result['sheduled_payment'] = $payment;
    $result['sheduled_number_of_payments'] = $periods;

    while ($endBalance > 0) {
      $beginBalance = $endBalance;
      $interest = $beginBalance * ($rate / $paymentsPerYear);
      $principal = $payment - $interest;
      $endBalance = $beginBalance - $principal - $extra + $lostPayment;
      $summaryInterest += $interest;
      $startDate->add($plusInterval);
      $paymentDate = $startDate->format('Y-m-d');

      if ($beginBalance + $interest <= $totalPayment) {
        $_left = $beginBalance + $interest;
        $payment = $_left >= $payment ? $payment : $_left;
        $extra = $_left - $payment;
        $totalPayment = $payment + $extra;
        $principal = $beginBalance;
        $endBalance = 0;
      }
      $totalExtra += $extra;

      $result['#rows'][$delta] = [
        'number' => $delta,
        'date' => $paymentDate,
        'begin_balance' => round($beginBalance, self::PRESICION),
        'sheduled_payment' => round($payment, self::PRESICION),
        'extra_payment' => round($extra, self::PRESICION),
        'total_payment' => round($totalPayment, self::PRESICION),
        'principal' => round($principal, self::PRESICION),
        'interest' => round($interest, self::PRESICION),
        'end_balance' => round($endBalance, self::PRESICION),
        'interest_summary' => round($summaryInterest, self::PRESICION),
      ];
      $delta++;
    }
    $result['actual_number_of_payments'] = $delta - 1;
    $result['total_early_payments'] = round($totalExtra, self::PRESICION);
    $result['total_interest'] = round($summaryInterest, self::PRESICION);

    return $result;
  }
}
