<?php

namespace Drupal\deko\Service;

/**
 * Class DeKoFinanceValidation.
 */
class DeKoFinanceValidation {

  // @TODO Consts can be moved to the config
  public const DEKO_MAX_YEARS = 30;
  public const DEKO_MAX_RATE = 30;
  public const DEKO_MIN_RATE = 0;
  public const DEKO_MAX_AMOUNT = 10000000;

  /**
   * Constructs a new DeKoFinanceValidation object.
   */
  public function __construct() {

  }

  public function checkLoanAmount($value):bool {
    return (is_numeric($value)) && $value > 0 && $value <= self::DEKO_MAX_AMOUNT;
  }

  public function checkRate($value): bool {
    return (is_numeric($value)) && $value >= self::DEKO_MIN_RATE && $value <= self::DEKO_MAX_RATE;
  }

  public function checkPeriodYears($value): bool {
    return filter_var($value, FILTER_VALIDATE_INT) && $value <= self::DEKO_MAX_YEARS;
  }

  public function checkPaymentsPerYear($value): bool {
    return filter_var($value, FILTER_VALIDATE_INT) && $value > 0;
  }

  public function checkStartDate($value): bool {
    $date = \DateTime::createFromFormat('Y-m-d', $value);
    return (bool) $date;
  }

  public function checkExtra($value): bool {
    return is_numeric($value) || empty($value);
  }
}
