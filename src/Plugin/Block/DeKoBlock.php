<?php

namespace Drupal\deko\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DeKoBlock' block.
 *
 * @Block(
 *  id = "deko_block",
 *  admin_label = @Translation("Test calculator block"),
 * )
 */
class DeKoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\deko\Form\DeKoCalculatorForm');

    return $form;
  }

}
