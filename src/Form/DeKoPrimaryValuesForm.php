<?php

namespace Drupal\deko\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\deko\Service\DeKoFinanceValidation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DeKoPrimaryValuesForm.
 *
 * This class has duplicate logic from DeKoCalculatorForm
 *
 * @see DeKoCalculatorForm
 */
class DeKoPrimaryValuesForm extends ConfigFormBase {

  protected $validation;

  public function __construct(ConfigFactoryInterface $config_factory, DeKoFinanceValidation $validation)
  {
    parent::__construct($config_factory);
    $this->validation = $validation;
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('deko.finance.validation')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'deko.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deko_primary_values_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('deko.settings');
    $form['loan_amount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Loan amount'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('settings.values.loan_amount'),
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('settings.values.rate'),
      '#weight' => '1',
      '#required' => TRUE,
    ];
    $form['period_in_years'] = [
      '#type' => 'number',
      '#title' => $this->t('Period in years'),
      '#default_value' => $config->get('settings.values.period_in_years'),
      '#weight' => '2',
      '#required' => TRUE,
    ];
    $form['payments_per_year'] = [
      '#type' => 'number',
      '#title' => $this->t('Payments per year'),
      '#default_value' => $config->get('settings.values.payments_per_year'),
      '#weight' => '3',
      '#required' => TRUE,
    ];
    $form['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#default_value' => $config->get('settings.values.start_date'),
      '#weight' => '4',
      '#required' => TRUE,
    ];
    $form['extra_payments'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Extra payments'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('settings.values.extra_payments'),
      '#weight' => '5',
    ];
    $form['lender_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lendee name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('settings.values.lender_name'),
      '#weight' => '5',
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    if (!$this->validation->checkLoanAmount($form_state->getValue('loan_amount'))) {
      $form_state->setError($form['loan_amount'], $this->t('Loan amount is incorrect (max is 10\'000\'000)'));
    }
    if (!$this->validation->checkRate($form_state->getValue('rate'))) {
      $errorVars = [
        '@min' => DeKoFinanceValidation::DEKO_MIN_RATE,
        '@max' => DeKoFinanceValidation::DEKO_MAX_RATE,
      ];
      $errorMessage = $this->t('Rate is incorrect (@min < rate < @max)', $errorVars);
      $form_state->setError($form['rate'], $errorMessage);
    }
    if (!$this->validation->checkPeriodYears($form_state->getValue('period_in_years'))) {
      $errorVars = [
        '@max' => DeKoFinanceValidation::DEKO_MAX_YEARS,
      ];
      $errorMessage = $this->t('Period in years is incorrect (period < @max)', $errorVars);
      $form_state->setError($form['period_in_years'], $errorMessage);
    }
    if (!$this->validation->checkPaymentsPerYear($form_state->getValue('payments_per_year'))) {
      $form_state->setError($form['payments_per_year'], $this->t('Payment per year value is incorrect'));
    }
    if (!$this->validation->checkStartDate($form_state->getValue('start_date'))) {
      $form_state->setError($form['start_date'], $this->t('Start date is incorrect'));
    }
    if (!$this->validation->checkExtra($form_state->getValue('extra_payments'))) {
      $form_state->setError($form['extra_payments'], $this->t('Extra payments is incorrect'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('deko.settings');
    foreach ($form_state->getValues() as $key => $value) {
      $config->set('settings.values.' . $key, $value);
    }
    $config->save();
  }

}
