<?php

namespace Drupal\deko\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\deko\Service\DeKoFinance;
use Drupal\deko\Service\DeKoFinanceValidation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DeKoCalculatorForm.
 */
class DeKoCalculatorForm extends FormBase {

  protected $finance;
  protected $logger;
  protected $validation;

  public function __construct(DeKoFinance $dekoFinance, LoggerChannelInterface $logger, DeKoFinanceValidation $validation) {
    $this->finance = $dekoFinance;
    $this->logger = $logger;
    $this->validation = $validation;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('deko.finance'),
      $container->get('logger.channel.deko'),
      $container->get('deko.finance.validation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deko_calculator_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('deko.settings');
    $form['loan_amount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Loan amount'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $this->getRequest()->get('loan_amount') ?: $config->get('settings.values.loan_amount'),
      '#weight' => '0',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::validateLoanAmount',
        'event' => 'change',
        'progress' => [
          'type' => 'trobber',
          'message' => $this->t('Checking...')
        ],
      ],
      '#suffix' => '<div id="deko_loan_amount_validation"></div>',
    ];
    $form['rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $this->getRequest()->get('rate') ?: $config->get('settings.values.rate'),
      '#weight' => '1',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::validateRate',
        'event' => 'change',
        'progress' => [
          'type' => 'trobber',
          'message' => $this->t('Checking...')
        ],
      ],
      '#suffix' => '<div id="deko_rate_validation"></div>',
    ];
    $form['period_in_years'] = [
      '#type' => 'number',
      '#title' => $this->t('Period in years'),
      '#default_value' => $this->getRequest()->get('period_in_years') ?: $config->get('settings.values.period_in_years'),
      '#weight' => '2',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::validatePeriodYears',
        'event' => 'change',
        'progress' => [
          'type' => 'trobber',
          'message' => $this->t('Checking...')
        ],
      ],
      '#suffix' => '<div id="deko_years_validation"></div>',
    ];
    $form['payments_per_year'] = [
      '#type' => 'number',
      '#title' => $this->t('payments_per_year'),
      '#default_value' => $this->getRequest()->get('payments_per_year') ?: $config->get('settings.values.payments_per_year'),
      '#weight' => '3',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::validatePaymentsPerYear',
        'event' => 'change',
        'progress' => [
          'type' => 'trobber',
          'message' => $this->t('Checking...')
        ],
      ],
      '#suffix' => '<div id="deko_per_year_validation"></div>',
    ];
    $form['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#default_value' => $this->getRequest()->get('start_date') ?: $config->get('settings.values.start_date'),
      '#weight' => '4',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::validateStartDate',
        'event' => 'change',
        'progress' => [
          'type' => 'trobber',
          'message' => $this->t('Checking...')
        ],
      ],
      '#suffix' => '<div id="deko_date_validation"></div>',
    ];
    $form['extra_payments'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Optional extra payments'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $this->getRequest()->get('extra_payments') ?: $config->get('settings.values.extra_payments'),
      '#weight' => '5',
      '#ajax' => [
        'callback' => '::validateExtra',
        'event' => 'change',
        'progress' => [
          'type' => 'trobber',
          'message' => $this->t('Checking...')
        ],
      ],
      '#suffix' => '<div id="deko_extra_validation"></div>',
    ];
    $form['lender_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lender name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $this->getRequest()->get('lender_name') ?: $config->get('settings.values.lender_name'),
      '#weight' => '5',
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::ajaxCalculate',
        'event' => 'click',
        'progress' => [
          'type' => 'trobber',
        ]
      ],
      '#weight' => '50',
    ];
    $form['result'] = [
      '#prefix' => '<div id="deko_result_wrapper">',
      '#suffix' => '</div>',
      '#weight' => '99',
    ];
    $form['table'] = [
      '#prefix' => '<div id="deko_table_wrapper">',
      '#suffix' => '</div>',
      '#weight' => '100',
    ];

    return $form;
  }

  public function validateLoanAmount(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$this->validation->checkLoanAmount($form_state->getValue('loan_amount'))) {
      $error = $this->t('Loan amount is incorrect (max is @max)', ['@max' => DeKoFinanceValidation::DEKO_MAX_AMOUNT]);
      $response->addCommand(new HtmlCommand('#deko_loan_amount_validation', $error));
    }
    else {
      $response->addCommand(new HtmlCommand('#deko_loan_amount_validation', ''));
    }

    return $response;
  }

  public function validateRate(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$this->validation->checkRate($form_state->getValue('rate'))) {
      $errorVars = [
        '@min' => DeKoFinanceValidation::DEKO_MIN_RATE,
        '@max' => DeKoFinanceValidation::DEKO_MAX_RATE,
      ];
      $errorMessage = $this->t('Rate is incorrect (@min < rate < @max)', $errorVars);
      $response->addCommand(new HtmlCommand('#deko_rate_validation', $errorMessage));
    }
    else {
      $response->addCommand(new HtmlCommand('#deko_rate_validation', ''));
    }

    return $response;
  }

  public function validatePeriodYears(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$this->validation->checkPeriodYears($form_state->getValue('period_in_years'))) {
      $errorVars = [
        '@max' => DeKoFinanceValidation::DEKO_MAX_YEARS,
      ];
      $errorMessage = $this->t('Period in years is incorrect (period < @max)', $errorVars);
      $response->addCommand(new HtmlCommand('#deko_years_validation', $errorMessage));
    }
    else {
      $response->addCommand(new HtmlCommand('#deko_years_validation', ''));
    }

    return $response;
  }

  public function validatePaymentsPerYear(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$this->validation->checkPaymentsPerYear($form_state->getValue('payments_per_year'))) {
      $errorMessage = $this->t('Payment per year value is incorrect');
      $response->addCommand(new HtmlCommand('#deko_per_year_validation', $errorMessage));
    }
    else {
      $response->addCommand(new HtmlCommand('#deko_per_year_validation', ''));
    }

    return $response;
  }

  public function validateStartDate(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$this->validation->checkStartDate($form_state->getValue('start_date'))) {
      $errorMessage = $this->t('Start date is incorrect');
      $response->addCommand(new HtmlCommand('#deko_date_validation', $errorMessage));
    }
    else {
      $response->addCommand(new HtmlCommand('#deko_date_validation', ''));
    }

    return $response;
  }

  public function validateExtra(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$this->validation->checkExtra($form_state->getValue('extra_payments'))) {
      $errorMessage = $this->t('Extra payments is incorrect');
      $response->addCommand(new HtmlCommand('#deko_extra_validation', $errorMessage));
    }
    else {
      $response->addCommand(new HtmlCommand('#deko_extra_validation', ''));
    }

    return $response;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (!$this->validation->checkLoanAmount($form_state->getValue('loan_amount'))) {
      $error = $this->t('Loan amount is incorrect (max is @max)', ['@max' => DeKoFinanceValidation::DEKO_MAX_AMOUNT]);
      $form_state->setErrorByName('loan_amount', $error);
    }
    if (!$this->validation->checkRate($form_state->getValue('rate'))) {
      $errorVars = [
        '@min' => DeKoFinanceValidation::DEKO_MIN_RATE,
        '@max' => DeKoFinanceValidation::DEKO_MAX_RATE,
      ];
      $errorMessage = $this->t('Rate is incorrect (@min < rate < @max)', $errorVars);
      $form_state->setErrorByName('rate', $errorMessage);
    }
    if (!$this->validation->checkPeriodYears($form_state->getValue('period_in_years'))) {
      $errorVars = [
        '@max' => DeKoFinanceValidation::DEKO_MAX_YEARS,
      ];
      $errorMessage = $this->t('Period in years is incorrect (period < @max)', $errorVars);
      $form_state->setErrorByName('period_in_years', $errorMessage);
    }
    if (!$this->validation->checkPaymentsPerYear($form_state->getValue('payments_per_year'))) {
      $form_state->setErrorByName('payments_per_year', $this->t('Payment per year value is incorrect'));
    }
    if (!$this->validation->checkStartDate($form_state->getValue('start_date'))) {
      $form_state->setErrorByName('start_date', $this->t('Start date is incorrect'));
    }
    if (!$this->validation->checkExtra($form_state->getValue('extra_payments'))) {
      $form_state->setErrorByName('extra_payments', $this->t('Extra payments is incorrect'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do.
  }

  private function prepareTable(array $data) {
    $table = [
      '#theme' => 'table',
      '#rows' => $data['#rows'],
    ];
    $table['#header'] = [
      $this->t('#'),
      $this->t('Payment Date'),
      $this->t('Begining Balance'),
      $this->t('Sheduled payment'),
      $this->t('Extra payment'),
      $this->t('Total payment'),
      $this->t('Principal'),
      $this->t('Interest'),
      $this->t('Ending balance'),
      $this->t('Cumulative interest'),
    ];
    return $table;
  }

  private function prepareLoanSummary(array $data) {
    $resultRows = [
      [
        [
          'data' => $this->t('Loan summary'),
          'colspan' => 2,
        ],
      ],
      [$this->t('Sheduled payments'), $data['sheduled_payment']],
      [$this->t('Sheduled number of payments'), $data['sheduled_number_of_payments']],
      [$this->t('Actual number of payments'), $data['actual_number_of_payments']],
      [$this->t('Total early payments'), $data['total_early_payments']],
      [$this->t('Total interest'), $data['total_interest']],
    ];
    $resultOutput = [
      '#theme' => 'table',
      '#rows' => $resultRows,
    ];
    return $resultOutput;
  }

  private function prepareLogMessage($amount, $rate, $years, $perYear, $date, $extra, $lenderName, $data) {
    $message = [];
    $message[] = 'IP: @ip';
    $message[] = 'username - @username';
    $message[] = 'Entered values:';
    $message[] = 'Loan amount - @amount';
    $message[] = 'Annual interest rate - @rate%';
    $message[] = 'Loan period in years - @years';
    $message[] = 'Payments per year - @per_year';
    $message[] = 'Start date - @date';
    $message[] = 'Optional extra payments - @extra';
    $message[] = 'Lender name - @lender_name';
    $message[] = 'Loan summary:';
    $message[] = 'Sheduled payment - @payment';
    $message[] = 'Sheduled number of payments - @sheduled_number_of_payments';
    $message[] = 'Actual number of payments - @actual_number_of_payments';
    $message[] = 'Total extra payments - @total_extra';
    $message[] = 'Total interest - @total_interest';

    $message = implode('<br>', $message);
    $messageVars = [
      '@ip' => $this->getRequest()->getClientIp(),
      '@username' => $this->currentUser()->getAccountName(),
      '@amount' => $amount,
      '@rate' => $rate,
      '@years' => $years,
      '@per_year' => $perYear,
      '@date' => $date,
      '@extra' => $extra,
      '@lender_name' => $lenderName,
      '@payment' => $data['sheduled_payment'],
      '@sheduled_number_of_payments' => $data['sheduled_number_of_payments'],
      '@actual_number_of_payments' => $data['actual_number_of_payments'],
      '@total_extra' => $data['total_early_payments'],
      '@total_interest' => $data['total_interest'],
    ];
    $this->logger->info($message, $messageVars);
  }

  public function ajaxCalculate(array &$form, FormStateInterface $form_state) {
    if ($form_state->getErrors()) {
      return;
    }
    $amount = floatval($form_state->getValue('loan_amount'));
    $rate = floatval($form_state->getValue('rate'));
    $years = intval($form_state->getValue('period_in_years'));
    $perYear = intval($form_state->getValue('payments_per_year'));
    $date = new \DateTime($form_state->getValue('start_date'));
    $extra = floatval($form_state->getValue('extra_payments'));

    $data = $this->finance->prepareDataList($amount, $rate, $years, $perYear, $date, $extra);
    $this->prepareLogMessage($amount, $rate, $years, $perYear, $form_state->getValue('start_date'), $extra, $form_state->getValue('lender_name'), $data);

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#deko_table_wrapper', $this->prepareTable($data)));
    $response->addCommand(new HtmlCommand('#deko_result_wrapper', $this->prepareLoanSummary($data)));

    return $response;
  }

}
