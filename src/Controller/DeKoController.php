<?php

namespace Drupal\deko\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\deko\Service\DeKoFinance;
use Drupal\deko\Service\DeKoFinanceValidation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DeKoController.
 */
class DeKoController extends ControllerBase {

  /**
   * Drupal\deko\Service\DeKoFinance definition.
   *
   * @var \Drupal\deko\Service\DeKoFinance
   */
  protected $dekoFinance;

  /**
   * Drupal\deko\Service\DeKoFinanceValidation definition.
   *
   * @var \Drupal\deko\Service\DeKoFinanceValidation
   */
  protected $dekoFinanceValidation;


  /**
   * Constructs a new DeKoController object.
   */
  public function __construct(DeKoFinance $deko_finance, DeKoFinanceValidation $deko_finance_validation) {
    $this->dekoFinance = $deko_finance;
    $this->dekoFinanceValidation = $deko_finance_validation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('deko.finance'),
      $container->get('deko.finance.validation')
    );
  }

  /**
   * Some action for special route callback.
   */
  public function calculate(Request $request) {
    $amount = $request->request->get('amount');
    $rate = $request->request->get('rate');
    $years = $request->request->get('years');
    $perYear = $request->request->get('per_year');
    $date = $request->request->get('date');
    $extra = $request->request->get('extra');
    $errors = [];

    if (!$this->dekoFinanceValidation->checkLoanAmount($amount)) {
      $errors[] = $this->t('Amount is incorrect');
    }
    if (!$this->dekoFinanceValidation->checkRate($rate)) {
      $errors[] = $this->t('Rate is incorrect');
    }
    if (!$this->dekoFinanceValidation->checkPeriodYears($years)) {
      $errors[] = $this->t('Loan period in years is incorrect');
    }
    if (!$this->dekoFinanceValidation->checkPaymentsPerYear($perYear)) {
      $errors[] = $this->t('Payments per year is incorrect');
    }
    if (!$this->dekoFinanceValidation->checkStartDate($date)) {
      $errors[] = $this->t('Start date is incorrect');
    }
    if (!$this->dekoFinanceValidation->checkExtra($extra)) {
      $errors[] = $this->t('Extra is incorrect');
    }

    if ($errors) {
      return new JsonResponse($errors, 400);
    }
    $startDate = \DateTime::createFromFormat('Y-m-d', $date);
    $data = $this->dekoFinance->prepareDataList($amount, $rate, $years, $perYear, $startDate, $extra);
    return new JsonResponse($data, 200);
  }

}
